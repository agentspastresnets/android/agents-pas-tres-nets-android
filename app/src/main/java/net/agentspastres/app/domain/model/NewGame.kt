package net.agentspastres.app.domain.model

import com.google.gson.annotations.SerializedName

class NewGame(
    @SerializedName("creatorId")
    val userId: Int,

    @SerializedName("latitude")
    val lat: Double? = 48.8666,

    @SerializedName("longitude")
    val lng: Double? = 2.33
)