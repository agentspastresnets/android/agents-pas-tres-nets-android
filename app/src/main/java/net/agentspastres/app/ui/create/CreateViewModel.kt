package net.agentspastres.app.ui.create

import io.reactivex.functions.BiFunction
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import net.agentspastres.app.ui.base.BaseViewModel

class CreateViewModel(controller: CreateController) :
    BaseViewModel<CreateAction, CreateResult, CreateViewState>() {

    override var reducer =
        BiFunction<CreateViewState, CreateResult, CreateViewState> { currentState, result ->
            return@BiFunction when (result) {
                is CreateResult.GameCreated -> currentState.copy(game = result.game)
                else -> currentState
            }
        }

    init {
        controller.resultStream()
            .subscribeOn(Schedulers.single())
            .scan(CreateViewState(), reducer)
            .subscribe(state)
            .addTo(disposeBag)

        controller.subscribeToActions(actions)
            .addTo(disposeBag)
    }
}