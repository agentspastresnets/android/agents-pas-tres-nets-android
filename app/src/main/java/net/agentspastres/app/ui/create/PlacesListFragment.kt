package net.agentspastres.app.ui.create

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.uber.autodispose.AutoDispose.autoDisposable
import com.uber.autodispose.android.lifecycle.AndroidLifecycleScopeProvider
import kotlinx.android.synthetic.main.fragment_list_place.*
import net.agentspastres.app.R
import net.agentspastres.app.ui.base.BaseFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class PlacesListFragment : BaseFragment<PlacesListViewState>() {

    private val adapter = PlaceAdapter {}

    override fun render(viewState: PlacesListViewState) {
        adapter.submitList(viewState.places)
    }

    private val viewModel: PlacesListViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_list_place, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rvPlaces.adapter = adapter
        rvPlaces.layoutManager = LinearLayoutManager(requireContext())
        rvPlaces.itemAnimator = null
        rvPlaces.setHasFixedSize(true)
        viewModel.stateStream()
            .`as`(autoDisposable(AndroidLifecycleScopeProvider.from(this)))
            .subscribe(::render)

        viewModel.process(CreateAction.GetPlaces)
    }
}