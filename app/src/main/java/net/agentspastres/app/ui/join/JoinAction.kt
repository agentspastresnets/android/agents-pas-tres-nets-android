package net.agentspastres.app.ui.join

import net.agentspastres.app.domain.model.Game
import net.agentspastres.app.ui.base.Action

sealed class JoinAction : Action {
    object GetGames : JoinAction()
    class Join(val userId: Int, val gameId: Int) : JoinAction()
    class GameSelected(val game: Game) : JoinAction()
}