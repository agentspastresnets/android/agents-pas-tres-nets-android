package net.agentspastres.app

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_test.*

class TestFragment : Fragment() {

    private var userId = 1

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_test, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btPlaces.setOnClickListener(
            Navigation.createNavigateOnClickListener(
                R.id.action_testFragment_to_placesListFragmentTest,
                null
            )
        )

        btAccuse.setOnClickListener(
            Navigation.createNavigateOnClickListener(
                R.id.action_testFragment_to_accuseFragment,
                null
            )
        )

        btPartie.setOnClickListener(
            Navigation.createNavigateOnClickListener(
                R.id.action_testFragment_to_nav_party,
                null
            )
        )

        btVote.setOnClickListener(
            Navigation.createNavigateOnClickListener(
                R.id.action_testFragment_to_voteFragment,
                null
            )
        )

        btCreate.setOnClickListener {
            findNavController().navigate(
                R.id.action_testFragment_to_nav_create,
                Bundle().apply { putInt("userId", userId) })
        }

        btJoin.setOnClickListener {
            findNavController().navigate(
                R.id.action_testFragment_to_nav_join,
                Bundle().apply { putInt("userId", userId) })
        }

        updateUserId(userId)

        ibDown.setOnClickListener {
            if (userId > 1) {
                userId--
            }
            updateUserId(userId)
        }
        ibUp.setOnClickListener {
            if (userId < 3) {
                userId++
            }
            updateUserId(userId)
        }
    }

    fun updateUserId(id: Int) {
        tvUserId.text = id.toString()
    }

}