package net.agentspastres.app.ui.join

import net.agentspastres.app.domain.model.Game
import net.agentspastres.app.ui.base.ViewState

data class JoinViewState(
    val games: List<Game> = emptyList(),
    val gameJoined: Game? = null,
    val gameSelected: Game? = null
) : ViewState