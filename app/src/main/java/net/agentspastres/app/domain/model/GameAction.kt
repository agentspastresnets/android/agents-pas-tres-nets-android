package net.agentspastres.app.domain.model

import org.threeten.bp.OffsetDateTime

data class GameAction(
    val id: Int,
    val initierId: Int,
    val targetId: Int?,
    val actionType: Type,
    val startDate: OffsetDateTime,
    val endDate: OffsetDateTime?
) {
    enum class Type {
        ASK_QUESTION
    }
}