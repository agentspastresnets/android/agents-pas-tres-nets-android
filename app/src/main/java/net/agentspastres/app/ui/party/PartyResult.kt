package net.agentspastres.app.ui.party

import net.agentspastres.app.domain.model.Game
import net.agentspastres.app.domain.model.Place
import net.agentspastres.app.domain.model.User
import net.agentspastres.app.ui.base.Result
import net.agentspastres.app.domain.model.Card as CardModel

sealed class PartyResult : Result {
    class Card(val card: CardModel) : PartyResult()
    class PlaceResult(val place: Place) : PartyResult()
    object Pause : PartyResult()
    object Resume : PartyResult()
    object Started : PartyResult()
    class SelectablePlayers(val playerIds: List<Int>) : PartyResult()
    class CurrentGame(val game: Game) : PartyResult()
    class Waiting(val creatorId: Int) : PartyResult()
    class PlayersResult(val players: List<User>) : PartyResult()
    class Accuse(val user: User) : PartyResult()
    class OtherPlayersResult(val players: List<User>) : PartyResult()
}