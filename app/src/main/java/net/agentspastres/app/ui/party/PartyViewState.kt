package net.agentspastres.app.ui.party

import net.agentspastres.app.domain.model.Card
import net.agentspastres.app.domain.model.Game
import net.agentspastres.app.domain.model.Place
import net.agentspastres.app.domain.model.User
import net.agentspastres.app.ui.base.ViewState

data class PartyViewState(
    val card: Card? = null,
    val place: Place? = null,
    val game: Game? = null,
    val actions: List<String> = emptyList(),
    val timer: String = "",
    val isPaused: Boolean = false,
    val players: List<User> = emptyList(),
    val selectablePlayers: List<Int> = emptyList()
) : ViewState