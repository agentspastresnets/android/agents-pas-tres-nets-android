package net.agentspastres.app.domain.model

data class Card(
    val place: Place,
    val job: String? = null
)