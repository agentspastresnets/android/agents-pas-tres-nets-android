package net.agentspastres.app.domain.model

data class TwoIds(
    val gameId: Int,
    val entityId: Int
)