package net.agentspastres.app.ui.create

import net.agentspastres.app.domain.model.Game
import net.agentspastres.app.domain.model.Place
import net.agentspastres.app.ui.base.Result

sealed class CreateResult : Result {
    class PlacesResult(val places: List<Place>) : CreateResult()
    class GameCreated(val game: Game) : CreateResult()
}