package net.agentspastres.app.ui.party

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import net.agentspastres.app.domain.model.AskEvent
import net.agentspastres.app.domain.model.JoinEvent
import net.agentspastres.app.domain.model.ListEvent
import net.agentspastres.app.domain.model.StartEvent
import net.agentspastres.app.repository.PartyRepository
import net.agentspastres.app.ui.base.Controller
import timber.log.Timber

class PartyController(private val partyRepository: PartyRepository) :
    Controller<PartyAction, PartyResult>() {

    override fun processImpl(action: PartyAction) {
        when (action) {
            is PartyAction.Init -> {
                partyRepository.getEvents(action.gameId).subscribe({ event ->
                    Timber.d(event.toString())
                    when (event) {
                        is JoinEvent -> {
                            GlobalScope.launch(Dispatchers.IO) {
                                val game = event.content.game
                                val players = game.playersId.map { partyRepository.getUser(it) }
                                resultRelay.accept(PartyResult.PlayersResult(players))
                            }
                        }
                        is StartEvent -> resultRelay.accept(PartyResult.Started)
                        is ListEvent -> resultRelay.accept(PartyResult.SelectablePlayers(event.content.list))
                        is AskEvent -> {
                            resultRelay.accept(PartyResult.SelectablePlayers(emptyList()))
                            resultRelay.accept(PartyResult.CurrentGame(event.content.game))
                        }
                    }
                }, Timber::e)
            }
            is PartyAction.Start -> {
                GlobalScope.launch(Dispatchers.IO) {
                    partyRepository.start(action.gameId)
                }
            }
            is PartyAction.GetCard -> {
                GlobalScope.launch(Dispatchers.IO) {
                    val place = partyRepository.getPlace(action.placeId)
                    resultRelay.accept(PartyResult.PlaceResult(place))
                }
            }
            is PartyAction.GetGame -> {
                GlobalScope.launch(Dispatchers.IO) {
                    val game = partyRepository.getGame(action.gameId)
                    resultRelay.accept(PartyResult.CurrentGame(game))
                    val players = game.playersId.map { partyRepository.getUser(it) }
                    resultRelay.accept(PartyResult.PlayersResult(players))
                }
            }
            is PartyAction.Pause -> resultRelay.accept(PartyResult.Pause)
            is PartyAction.Resume -> resultRelay.accept(PartyResult.Resume)
            is PartyAction.GetPlayers -> {
                GlobalScope.launch(Dispatchers.IO) {
                    val players = action.userIds.map { partyRepository.getUser(it) }
                    resultRelay.accept(PartyResult.PlayersResult(players))
                }
            }
            is PartyAction.NextTurn -> {
                GlobalScope.launch(Dispatchers.IO) {
                    partyRepository.nextTurn(action.gameId)
                }
            }
            is PartyAction.AskQuestion -> {
                GlobalScope.launch(Dispatchers.IO) {
                    partyRepository.askQuestionTo(action.gameId, action.userSelected)
                }
            }
            is PartyAction.GetOtherPlayers -> {
                GlobalScope.launch(Dispatchers.IO) {
                    val game = partyRepository.getGame(action.gameId)
                    val players = game.playersId.filter { it != action.userId }
                        .map { partyRepository.getUser(it) }
                    resultRelay.accept(PartyResult.OtherPlayersResult(players))
                }
            }
            is PartyAction.Accuse -> resultRelay.accept(PartyResult.Accuse(action.user))
        }
    }
}