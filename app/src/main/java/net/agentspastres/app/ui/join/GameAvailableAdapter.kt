package net.agentspastres.app.ui.join

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_game_selectable.view.*
import net.agentspastres.app.R
import net.agentspastres.app.domain.model.Game
import net.agentspastres.app.ui.join.GameAvailableAdapter.GameViewHolder

class GameAvailableAdapter(val selectionListener: (game: Game) -> Unit) :
    ListAdapter<Game, GameViewHolder>(DIFF_CALLBACK) {

    private var currentSelectedGame: Int = -1

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Game>() {
            override fun areContentsTheSame(oldItem: Game, newItem: Game): Boolean {
                return oldItem == newItem
            }

            override fun areItemsTheSame(oldItem: Game, newItem: Game): Boolean {
                return oldItem.id == newItem.id
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GameViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_game_selectable, parent, false)
        return GameViewHolder(view)
    }

    override fun onBindViewHolder(holder: GameViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    fun setSelectedGame(position: Int) {
        val oldSelectedGame = currentSelectedGame
        currentSelectedGame = position
        notifyItemChanged(oldSelectedGame)
        notifyItemChanged(currentSelectedGame)
    }

    inner class GameViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(game: Game) {
            itemView.rvGame.text = game.id.toString()
            itemView.rvGame.isChecked = adapterPosition == currentSelectedGame
            itemView.tvGameUser.text = "${game.playersId.size}/8"
            itemView.rvGame.setOnClickListener { selectionListener(game) }
        }
    }
}