package net.agentspastres.app.ui.party.wait

import net.agentspastres.app.domain.model.User
import net.agentspastres.app.ui.base.ViewState

data class WaitViewState(
    val displayStart: Boolean = false,
    val enableStart: Boolean = false,
    val users: List<User> = emptyList(),
    val started: Boolean = false
) : ViewState