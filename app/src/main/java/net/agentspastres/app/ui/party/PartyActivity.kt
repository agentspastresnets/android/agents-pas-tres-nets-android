package net.agentspastres.app.ui.party

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import net.agentspastres.app.R

class PartyActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_party)
        supportFragmentManager.beginTransaction()
            .add(R.id.frameContainer, PartyFragment())
            .commit()
    }
}
