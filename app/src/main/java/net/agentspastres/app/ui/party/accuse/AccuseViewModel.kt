package net.agentspastres.app.ui.party.accuse

import io.reactivex.functions.BiFunction
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import net.agentspastres.app.ui.base.BaseViewModel
import net.agentspastres.app.ui.party.PartyAction
import net.agentspastres.app.ui.party.PartyController
import net.agentspastres.app.ui.party.PartyResult

class AccuseViewModel(controller: PartyController) :
    BaseViewModel<PartyAction, PartyResult, AccuseViewState>() {
    override var reducer =
        BiFunction<AccuseViewState, PartyResult, AccuseViewState> { currentState, result ->
            return@BiFunction when (result) {
                is PartyResult.OtherPlayersResult -> currentState.copy(otherPlayers = result.players)
                is PartyResult.Accuse -> currentState.copy(selectedUser = result.user)
                else -> currentState
            }
        }

    init {
        controller.resultStream()
            .subscribeOn(Schedulers.single())
            .scan(AccuseViewState(), reducer)
            .subscribe(state)
            .addTo(disposeBag)

        controller.subscribeToActions(actions)
            .addTo(disposeBag)
    }
}