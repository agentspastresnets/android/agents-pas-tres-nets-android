package net.agentspastres.build

object Versions {
    const val androidTools = "3.3.0"

    object AndroidX {
        const val appcompat = "1.1.0-alpha01"
        const val cardView = "1.0.0"
        const val constraintLayout = "2.0.0-alpha3"
        const val navigation = "1.0.0-alpha09"
        const val recyclerview = "1.0.0"
        const val testRunner = "1.1.1"
        const val testEspressoCore = "3.1.1"
    }

    const val glide = "4.8.0"
    const val gson = "2.8.5"
    const val junit = "4.12"
    const val koin = "1.0.2"
    const val kotlin = "1.3.11"
    const val kotlinCoroutines = "1.1.0"
    const val ktlint = "0.29.0"
    const val ktlintGradle = "6.3.1"
    const val retrofit = "2.5.0"
    const val rxAndroid = "2.1.0"
    const val rxJava = "2.2.5"
    const val rxKotlin = "2.3.0"
    const val rxRelay = "2.1.0"
    const val scarlet = "0.2.1-alpha4"
    const val versionsPlugin = "0.20.0"
}

object Libs {
    object AndroidX {
        val appcompat = "androidx.appcompat:appcompat:${Versions.AndroidX.appcompat}"
        val cardView = "androidx.cardview:cardview:${Versions.AndroidX.cardView}"
        val constraintLayout =
            "androidx.constraintlayout:constraintlayout:${Versions.AndroidX.constraintLayout}"
        val navigationFragment =
            "android.arch.navigation:navigation-fragment-ktx:${Versions.AndroidX.navigation}"
        val navigationUi =
            "android.arch.navigation:navigation-ui-ktx:${Versions.AndroidX.navigation}"
        val recyclerview = "androidx.recyclerview:recyclerview:${Versions.AndroidX.recyclerview}"
    }

    val glide = "com.github.bumptech.glide:glide:${Versions.glide}"
    val glideCompiler = "com.github.bumptech.glide:compiler:${Versions.glide}"
    val gson = "com.google.code.gson:gson:${Versions.gson}"
    val koinAndroid = "org.koin:koin-android:${Versions.koin}"
    val koinAndroidX = "org.koin:koin-androidx-scope:${Versions.koin}"
    val koinCore = "org.koin:koin-core:${Versions.koin}"
    val koinViewModel = "org.koin:koin-androidx-viewmodel:${Versions.koin}"
    val kotlinJdk = "org.jetbrains.kotlin:kotlin-stdlib-jdk8:${Versions.kotlin}"
    val kotlinCoroutines =
        "org.jetbrains.kotlinx:kotlinx-coroutines-core:${Versions.kotlinCoroutines}"
    val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit}"
    val retrofitAdapterRxJava2 = "com.squareup.retrofit2:adapter-rxjava2:${Versions.retrofit}"
    val retrofitConverterGson = "com.squareup.retrofit2:converter-gson:${Versions.retrofit}"
    val rxAndroid = "io.reactivex.rxjava2:rxandroid:${Versions.rxAndroid}"
    val rxJava = "io.reactivex.rxjava2:rxjava:${Versions.rxJava}"
    val rxKotlin = "io.reactivex.rxjava2:rxkotlin:${Versions.rxKotlin}"
    val rxRelay = "com.jakewharton.rxrelay2:rxrelay:${Versions.rxRelay}"
    val scarlet = "com.github.tinder:scarlet:${Versions.scarlet}"
    val scarletAdapterRxJava =
        "com.github.tinder:scarlet-stream-adapter-rxjava2:${Versions.scarlet}"
    val scarletAdapterGson = "com.github.tinder:scarlet-stream-adapter-gson:${Versions.scarlet}"

    object Test {
        val junit = "junit:junit:${Versions.junit}"
    }

    object AndroidTest {
        val runner = "androidx.test:runner:${Versions.AndroidX.testRunner}"
        val espressoCore =
            "androidx.test.espresso:espresso-core:${Versions.AndroidX.testEspressoCore}"
    }
}