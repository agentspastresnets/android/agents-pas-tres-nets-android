package net.agentspastres.app.domain.model

data class User(
    val id: Int,
    val name: String,
    val picture: String = "",
    val mail: String = ""
)