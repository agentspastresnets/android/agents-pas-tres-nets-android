package net.agentspastres.app.ui.base

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import io.reactivex.disposables.CompositeDisposable

abstract class BaseFragment<State : ViewState> : Fragment() {

    protected val viewDisposable: CompositeDisposable = CompositeDisposable()

    abstract fun render(viewState: State)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }
}