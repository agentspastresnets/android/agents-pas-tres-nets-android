package net.agentspastres.app.ui.party

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.navigation.Navigation
import com.bumptech.glide.Glide
import com.uber.autodispose.AutoDispose
import com.uber.autodispose.android.lifecycle.AndroidLifecycleScopeProvider
import kotlinx.android.synthetic.main.fragment_party.*
import kotlinx.android.synthetic.main.layout_card.*
import net.agentspastres.app.R
import net.agentspastres.app.domain.model.GameAction
import net.agentspastres.app.domain.model.User
import net.agentspastres.app.ui.base.BaseFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class PartyFragment : BaseFragment<PartyViewState>() {

    private val viewModel: PartyViewModel by viewModel()

    private var gameId: Int = -1
    private var userId: Int = -1

    override fun render(viewState: PartyViewState) {
        if (viewState.isPaused) {
            cartImage.visibility = View.INVISIBLE
            btPause.setText(R.string.resume)
        } else {
            cartImage.visibility = View.VISIBLE
            btPause.setText(R.string.pause)
        }

        if (viewState.place != null) {
            Glide.with(this)
                .load(viewState.place.picture)
                .into(cartImage)
        } else if (viewState.game != null && viewState.game.spyId != userId) {
            viewModel.process(PartyAction.GetCard(viewState.game.placeId, userId))
        }

        if (viewState.game != null) {
            btAction.visibility = View.INVISIBLE
            val lastAction = viewState.game.actions.last()
            when (lastAction.actionType) {
                GameAction.Type.ASK_QUESTION -> askQuestionAction(lastAction, viewState.players)
            }
        }

        if (viewState.selectablePlayers.isNotEmpty()) {
            btAction.visibility = View.INVISIBLE
            tvAction.text = ""
            val lastTarget = viewState.game?.actions?.last()?.targetId
            selectablePlayers(lastTarget, viewState.selectablePlayers, viewState.players)
        }
    }

    private fun askQuestionAction(action: GameAction, users: List<User>) {
        val initierName =
            users.find { it.id == action.initierId }?.name ?: action.initierId.toString()

        val targetName = users.find { it.id == action.targetId }?.name ?: action.targetId.toString()

        if (action.initierId == userId) {
            if (action.targetId != null) {
                tvAction.text = "Vous pouvez poser une question à $targetName."
                btAction.visibility = View.VISIBLE
                btAction.text = "Fait"
            } else {
                tvAction.text = "A qui voulez-vous poser une question ?"
            }
        } else if (action.targetId == userId) {
            tvAction.text = "$initierName vous pose une question."
        } else {
            if (action.targetId != null) {
                tvAction.text = "$initierName pose une question à $targetName."
            } else {
                tvAction.text = "En attente de $initierName."
            }
        }
    }

    private fun selectablePlayers(
        lastTarget: Int?,
        selectablePlayers: List<Int>,
        players: List<User>
    ) {
        if (lastTarget == userId) {
            var userSelected = selectablePlayers[0]
            AlertDialog.Builder(requireContext())
                .setTitle("Selectionner la personne à qui vous souhaitez poser une question :")
                .setSingleChoiceItems(selectablePlayers.map { playerId ->
                    players.find { it.id == playerId }?.name ?: playerId.toString()
                }.toTypedArray(), 0) { dialog, which ->
                    userSelected = selectablePlayers[which]
                }
                .setPositiveButton("Valider") { dialog, which ->
                    viewModel.process(PartyAction.AskQuestion(gameId, userSelected))
                }
                .setCancelable(false)
                .create()
                .show()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_party, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        gameId = arguments?.getInt("gameId") ?: -1
        userId = arguments?.getInt("userId") ?: -1

        viewModel.stateStream()
            .`as`(AutoDispose.autoDisposable(AndroidLifecycleScopeProvider.from(this)))
            .subscribe(::render)

        btPause.setOnClickListener {
            if (viewModel.currentState()?.isPaused == true) {
                viewModel.process(PartyAction.Resume)
            } else {
                viewModel.process(PartyAction.Pause)
            }
        }

        btAccuse.setOnClickListener(
            Navigation.createNavigateOnClickListener(
                R.id.action_partyFragment2_to_accuseFragment2,
                Bundle().apply {
                    putInt("gameId", gameId)
                    putInt("userId", userId)
                }
            )
        )

        btAction.setOnClickListener {
            viewModel.process(PartyAction.NextTurn(gameId))
        }

        viewModel.process(PartyAction.GetGame(gameId))
    }
}