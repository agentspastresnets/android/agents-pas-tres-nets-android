package net.agentspastres.app.ui.join

import net.agentspastres.app.domain.model.Game
import net.agentspastres.app.ui.base.Result

sealed class JoinResult : Result {
    class GamesResult(val games: List<Game>) : JoinResult()
    class GameJoined(val game: Game) : JoinResult()
    class GameSelected(val game: Game) : JoinResult()

}