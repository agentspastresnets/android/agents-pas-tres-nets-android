package net.agentspastres.app.ui.base

import androidx.lifecycle.ViewModel
import com.jakewharton.rxrelay2.BehaviorRelay
import com.jakewharton.rxrelay2.PublishRelay
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.BiFunction

abstract class BaseViewModel<A : Action, R : Result, State : ViewState> : ViewModel() {

    protected val disposeBag = CompositeDisposable()
    protected val state: BehaviorRelay<State> = BehaviorRelay.create()
    protected val actions = PublishRelay.create<A>()

    protected abstract var reducer: BiFunction<State, R, State>

    fun process(action: A) {
        actions.accept(action)
    }

    fun stateStream(): Observable<State> {
        return state.observeOn(AndroidSchedulers.mainThread())
    }

    fun currentState(): State? {
        return state.value
    }

    override fun onCleared() {
        super.onCleared()
        disposeBag.clear()
    }
}