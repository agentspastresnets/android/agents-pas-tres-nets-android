package net.agentspastres.app.domain.model

import org.threeten.bp.OffsetDateTime

data class Game(
    val id: Int,
    val status: Status,
    val endDate: OffsetDateTime,
    val playersId: List<Int>,
    val creatorId: Int,
    val placeId: Int,
    val spyId: Int,
    val actions: List<GameAction>,
    val latitude: Double?,
    val longitude: Double?
) {
    enum class Status {
        IN_PROGRESS,
        PAUSED,
        WAIT_FOR_PLAYER,
        SPY_WON,
        AGENTS_WON,
        CANCELED
    }
}