package net.agentspastres.app.ui.join

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.uber.autodispose.AutoDispose
import com.uber.autodispose.android.lifecycle.AndroidLifecycleScopeProvider
import kotlinx.android.synthetic.main.fragment_join.*
import net.agentspastres.app.R
import net.agentspastres.app.R.layout
import net.agentspastres.app.ui.base.BaseFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class JoinPartyFragment : BaseFragment<JoinViewState>() {

    private val viewModel: JoinViewModel by viewModel()

    private val adapter = GameAvailableAdapter { viewModel.process(JoinAction.GameSelected(it)) }

    private var userId: Int = -1

    override fun render(viewState: JoinViewState) {
        adapter.submitList(viewState.games)

        btJoin.isEnabled = viewState.gameSelected != null &&
                !viewState.gameSelected.playersId.contains(userId)

        viewState.gameSelected?.let {
            val index = viewState.games.indexOf(it)
            if (index != -1) {
                adapter.setSelectedGame(index)
            }
        }

        if (viewState.gameJoined != null) {
            findNavController().navigate(
                R.id.action_joinPartyFragment_to_nav_party,
                Bundle().apply {
                    putInt("gameId", viewState.gameJoined.id)
                    putInt("userId", userId)
                })
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(layout.fragment_join, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.stateStream()
            .`as`(AutoDispose.autoDisposable(AndroidLifecycleScopeProvider.from(this)))
            .subscribe(::render)

        viewModel.process(JoinAction.GetGames)

        rvGames.adapter = adapter
        rvGames.layoutManager = LinearLayoutManager(requireContext())
        rvGames.itemAnimator = null
        rvGames.setHasFixedSize(true)

        userId = arguments?.getInt("userId") ?: -1

        btJoin.setOnClickListener {
            val gameSelected = viewModel.currentState()?.gameSelected?.id
            if (gameSelected != null && gameSelected != -1) {
                viewModel.process(JoinAction.Join(userId, gameSelected))
            }
        }
    }
}
