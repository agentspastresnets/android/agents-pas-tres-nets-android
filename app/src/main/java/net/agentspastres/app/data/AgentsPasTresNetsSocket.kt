package net.agentspastres.app.data

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import ua.naiksoftware.stomp.Stomp
import ua.naiksoftware.stomp.StompClient
import ua.naiksoftware.stomp.dto.LifecycleEvent


interface AgentsPasTresNetsSocket {
    fun connect(gameId: Int): Observable<String>
}

class AgentsPasTresNetsSocketImpl : AgentsPasTresNetsSocket {

    private val stompClient: StompClient = Stomp.over(
        Stomp.ConnectionProvider.OKHTTP,
        "https://aptn.redissi.xyz/api/games/ws/socket/websocket"
    )

    override fun connect(gameId: Int): Observable<String> {
        stompClient.withClientHeartbeat(30000).withServerHeartbeat(30000)
        stompClient.connect()

        stompClient.lifecycle()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { lifecycleEvent ->
                when (lifecycleEvent.type) {
                    LifecycleEvent.Type.OPENED -> Timber.d("Stomp connection opened")
                    LifecycleEvent.Type.ERROR -> {
                        Timber.e(lifecycleEvent.exception)
                    }
                    LifecycleEvent.Type.CLOSED -> Timber.d("Stomp connection closed")
                    LifecycleEvent.Type.FAILED_SERVER_HEARTBEAT -> Timber.d("Stomp failed server heartbeat")
                }
            }

        return stompClient.topic("/topic/messages/$gameId").map { it.payload }.toObservable()

//        return Observable.create { emitter ->
//            val ws = WebSocketFactory()
//                .setConnectionTimeout(5000)
//                .createSocket("https://aptn.redissi.xyz/api/games/ws/socket/topic/messages/$gameId")
//                .addListener(object : WebSocketAdapter() {
//                    override fun onTextMessage(websocket: WebSocket, text: String) {
//                        super.onTextMessage(websocket, text)
//                        emitter.onNext(text)
//                    }
//
//                    override fun onConnectError(
//                        websocket: WebSocket,
//                        exception: WebSocketException
//                    ) {
//                        super.onConnectError(websocket, exception)
//                        emitter.onError(exception)
//                    }
//
//                    override fun onConnected(
//                        websocket: WebSocket?,
//                        headers: MutableMap<String, MutableList<String>>?
//                    ) {
//                        super.onConnected(websocket, headers)
//                        Timber.d("ws connected")
//                    }
//                })
//                .addExtension(WebSocketExtension.PERMESSAGE_DEFLATE)
//                .connect()
//        }
    }
}