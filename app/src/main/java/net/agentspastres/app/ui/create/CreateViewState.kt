package net.agentspastres.app.ui.create

import net.agentspastres.app.domain.model.Game
import net.agentspastres.app.ui.base.ViewState

data class CreateViewState(
    val game: Game? = null
) : ViewState