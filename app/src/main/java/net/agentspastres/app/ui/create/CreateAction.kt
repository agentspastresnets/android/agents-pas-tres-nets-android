package net.agentspastres.app.ui.create

import net.agentspastres.app.ui.base.Action

sealed class CreateAction : Action {
    object GetPlaces : CreateAction()
    class Create(val userId: Int) : CreateAction()
}