package net.agentspastres.app.data

import kotlinx.coroutines.Deferred
import net.agentspastres.app.domain.model.Game
import net.agentspastres.app.domain.model.NewGame
import net.agentspastres.app.domain.model.Place
import net.agentspastres.app.domain.model.TwoIds
import net.agentspastres.app.domain.model.User
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface AgentsPasTresNetsService {

    @GET("places")
    fun getPlaces(): Deferred<List<Place>>

    @GET("places/{placeId}")
    fun getPlace(@Path("placeId") placeId: Int): Deferred<Place>

    @POST("games/create")
    fun createGame(@Body newGame: NewGame): Deferred<Game>

    @POST("games/join")
    fun join(@Body twoIds: TwoIds): Deferred<Game>

    @GET("games/available")
    fun getAvailableGames(): Deferred<List<Game>>

    @GET("games/start/{gameId}")
    fun start(@Path("gameId") gameId: Int): Deferred<Game>

    @GET("games/{gameId}")
    fun getGame(@Path("gameId") gameId: Int): Deferred<Game>

    @GET("users/{userId}")
    fun getUser(@Path("userId") userId: Int): Deferred<User>

    @GET("games/nextTurn/{gameId}")
    fun nextTurn(@Path("gameId") gameId: Int): Deferred<Unit>

    @POST("games/newAskQuestion")
    fun askQuestion(@Body twoIds: TwoIds): Deferred<Unit>

    @GET("games/spyTry")
    fun spyTry(@Body twoIds: TwoIds): Deferred<Game>
}