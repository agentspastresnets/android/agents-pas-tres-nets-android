package net.agentspastres.app.di

import net.agentspastres.app.repository.PartyRepository
import net.agentspastres.app.repository.PartyRepositoryImpl
import net.agentspastres.app.ui.party.PartyController
import net.agentspastres.app.ui.party.PartyViewModel
import net.agentspastres.app.ui.party.accuse.AccuseViewModel
import net.agentspastres.app.ui.party.wait.WaitViewModel
import org.koin.androidx.viewmodel.experimental.builder.viewModel
import org.koin.dsl.module.module

val partyModule = module {

    single<PartyRepository> { PartyRepositoryImpl(get(), get(), get()) }

    single { PartyController(get()) }
    viewModel<PartyViewModel>()

    viewModel<AccuseViewModel>()

    viewModel<WaitViewModel>()
}