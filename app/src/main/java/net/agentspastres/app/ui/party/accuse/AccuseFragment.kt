package net.agentspastres.app.ui.party.accuse

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.uber.autodispose.AutoDispose
import com.uber.autodispose.android.lifecycle.AndroidLifecycleScopeProvider
import kotlinx.android.synthetic.main.fragment_accuse.*
import net.agentspastres.app.R
import net.agentspastres.app.ui.base.BaseFragment
import net.agentspastres.app.ui.party.PartyAction
import org.koin.androidx.viewmodel.ext.android.viewModel

class AccuseFragment : BaseFragment<AccuseViewState>() {

    private val viewModel: AccuseViewModel by viewModel()

    private val adapter = AccuseAdapter { viewModel.process(PartyAction.Accuse(it)) }

    private var gameId: Int = -1
    private var userId: Int = -1

    override fun render(viewState: AccuseViewState) {
        adapter.submitList(viewState.otherPlayers)
        viewState.selectedUser?.let {
            val index = viewState.otherPlayers.indexOf(it)
            if (index != -1) {
                adapter.setSelectedUser(index)
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_accuse, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        gameId = arguments?.getInt("gameId") ?: -1
        userId = arguments?.getInt("userId") ?: -1

        rvAccuse.adapter = adapter
        rvAccuse.layoutManager = LinearLayoutManager(requireContext())
        rvAccuse.itemAnimator = null
        rvAccuse.setHasFixedSize(true)

        viewModel.stateStream()
            .`as`(AutoDispose.autoDisposable(AndroidLifecycleScopeProvider.from(this)))
            .subscribe(::render)

        viewModel.process(PartyAction.GetOtherPlayers(gameId, userId))
    }
}