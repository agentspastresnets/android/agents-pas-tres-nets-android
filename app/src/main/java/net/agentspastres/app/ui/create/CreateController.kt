package net.agentspastres.app.ui.create

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import net.agentspastres.app.repository.PartyRepository
import net.agentspastres.app.ui.base.Controller

class CreateController(private val partyRepository: PartyRepository) :
    Controller<CreateAction, CreateResult>() {
    override fun processImpl(action: CreateAction) {
        when (action) {
            is CreateAction.GetPlaces -> getPlaces()
            is CreateAction.Create -> createGame(action.userId)
        }
    }

    private fun getPlaces() {
        GlobalScope.launch(Dispatchers.IO) {
            val places = partyRepository.getPlaces()
            resultRelay.accept(CreateResult.PlacesResult(places))
        }
    }

    private fun createGame(userId: Int) {
        GlobalScope.launch(Dispatchers.IO) {
            val game = partyRepository.createGame(userId, null, null)
            resultRelay.accept(CreateResult.GameCreated(game))
        }
    }

}