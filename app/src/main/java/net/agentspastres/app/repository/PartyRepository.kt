package net.agentspastres.app.repository

import com.google.gson.Gson
import io.reactivex.Observable
import net.agentspastres.app.data.AgentsPasTresNetsService
import net.agentspastres.app.data.AgentsPasTresNetsSocket
import net.agentspastres.app.domain.model.Card
import net.agentspastres.app.domain.model.Event
import net.agentspastres.app.domain.model.Game
import net.agentspastres.app.domain.model.NewGame
import net.agentspastres.app.domain.model.Place
import net.agentspastres.app.domain.model.TwoIds
import net.agentspastres.app.domain.model.User

interface PartyRepository {

    fun getEvents(gameId: Int): Observable<Event>

    fun getCard(): Card

    suspend fun getAvailableGames(): List<Game>

    suspend fun getPlaces(): List<Place>

    suspend fun getPlace(placeId: Int): Place

    suspend fun createGame(userId: Int, lat: Double?, lng: Double?): Game

    suspend fun join(userId: Int, gameId: Int): Game

    suspend fun start(gameId: Int): Game

    suspend fun getGame(gameId: Int): Game

    suspend fun getUser(userId: Int): User

    suspend fun nextTurn(gameId: Int)

    suspend fun askQuestionTo(gameId: Int, userId: Int)
}

class PartyRepositoryImpl(
    private val service: AgentsPasTresNetsService,
    private val socket: AgentsPasTresNetsSocket,
    private val gson: Gson
) : PartyRepository {

    override fun getEvents(gameId: Int): Observable<Event> {
        return socket.connect(gameId)
            .map {
                gson.fromJson(it, Event::class.java)
            }
    }

    override fun getCard(): Card {
        return Card(
            Place(
                picture = "https://news.files.bbci.co.uk/vj/live/idt-images/" +
                        "quizzes-Would_You_Fly_as_a_Spy/GettyImages-837345268-1920x1080_r3joa.jpg"
            )
        )
    }

    override suspend fun getPlaces(): List<Place> {
        return service.getPlaces().await()
    }

    override suspend fun getPlace(placeId: Int): Place {
        return service.getPlace(placeId).await()
    }

    override suspend fun getAvailableGames(): List<Game> {
        return service.getAvailableGames().await()
    }

    override suspend fun createGame(userId: Int, lat: Double?, lng: Double?): Game {
        return service.createGame(NewGame(userId, lat, lng)).await()
    }

    override suspend fun join(userId: Int, gameId: Int): Game {
        return service.join(TwoIds(gameId, userId)).await()
    }

    override suspend fun start(gameId: Int): Game {
        return service.start(gameId).await()
    }

    override suspend fun getGame(gameId: Int): Game {
        return service.getGame(gameId).await()
    }

    override suspend fun getUser(userId: Int): User {
        return service.getUser(userId).await()
    }

    override suspend fun nextTurn(gameId: Int) {
        service.nextTurn(gameId).await()
    }

    override suspend fun askQuestionTo(gameId: Int, userId: Int) {
        service.askQuestion(TwoIds(gameId, userId)).await()
    }
}