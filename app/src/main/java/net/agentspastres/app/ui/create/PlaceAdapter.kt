package net.agentspastres.app.ui.create

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.graphics.drawable.ColorDrawable
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_place.view.*
import net.agentspastres.app.R
import net.agentspastres.app.domain.model.Place
import net.agentspastres.app.ui.create.PlaceAdapter.PlaceViewHolder

class PlaceAdapter(val selectionListener: (places: List<Place>) -> Unit) :
    ListAdapter<Place, PlaceViewHolder>(DIFF_CALLBACK) {

    private val selectedPlaces: MutableList<Place> = arrayListOf()
    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Place>() {
            override fun areContentsTheSame(oldItem: Place, newItem: Place): Boolean {
                return oldItem == newItem
            }

            override fun areItemsTheSame(oldItem: Place, newItem: Place): Boolean {
                return oldItem.id == newItem.id
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlaceViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_place, parent, false)
        return PlaceViewHolder(view)
    }

    override fun onBindViewHolder(holder: PlaceViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class PlaceViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(place: Place) {
            val resources = itemView.context.resources
            itemView.tvPlace.text = place.name
            Glide.with(itemView)
                .load(place.picture)
                .into(itemView.ivPlace)
            itemView.setOnClickListener {
                if (selectedPlaces.contains(place)) {
                    selectedPlaces.remove(place)
                    itemView.flPlace.foreground =
                            ColorDrawable(
                                ResourcesCompat.getColor(
                                    resources,
                                    R.color.place_selected,
                                    null
                                )
                            )
                } else {
                    selectedPlaces.add(place)
                    itemView.flPlace.foreground = null
                }
                selectionListener(selectedPlaces)
            }
        }
    }
}