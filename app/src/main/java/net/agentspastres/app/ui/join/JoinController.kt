package net.agentspastres.app.ui.join

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import net.agentspastres.app.repository.PartyRepository
import net.agentspastres.app.ui.base.Controller

class JoinController(private val partyRepository: PartyRepository) :
    Controller<JoinAction, JoinResult>() {
    override fun processImpl(action: JoinAction) {
        when (action) {
            is JoinAction.GetGames -> getGames()
            is JoinAction.Join -> joinGame(action.userId, action.gameId)
            is JoinAction.GameSelected -> resultRelay.accept(JoinResult.GameSelected(action.game))
        }
    }

    private fun getGames() {
        GlobalScope.launch(Dispatchers.IO) {
            val games = partyRepository.getAvailableGames()
            resultRelay.accept(JoinResult.GamesResult(games))
        }
    }

    private fun joinGame(userId: Int, gameId: Int) {
        GlobalScope.launch(Dispatchers.IO) {
            val game = partyRepository.join(userId, gameId)
            resultRelay.accept(JoinResult.GameJoined(game))
        }
    }

}