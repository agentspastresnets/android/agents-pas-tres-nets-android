package net.agentspastres.app.ui.create

import net.agentspastres.app.domain.model.Place
import net.agentspastres.app.ui.base.ViewState

data class PlacesListViewState(
    val places: List<Place> = emptyList()
) : ViewState