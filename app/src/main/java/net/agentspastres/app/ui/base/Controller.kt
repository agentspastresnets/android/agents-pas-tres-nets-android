package net.agentspastres.app.ui.base

import com.jakewharton.rxrelay2.PublishRelay
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

abstract class Controller<A : Action, R : Result> {

    protected val resultRelay: PublishRelay<R> = PublishRelay.create()

    fun subscribeToActions(actionStream: Observable<A>): Disposable {
        return actionStream
            .observeOn(Schedulers.single())
            .subscribe(::processImpl)
    }

    fun resultStream(): Observable<R> = resultRelay

    protected abstract fun processImpl(action: A)
}