package net.agentspastres.app.di

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import net.agentspastres.app.data.AgentsPasTresNetsService
import net.agentspastres.app.data.AgentsPasTresNetsSocket
import net.agentspastres.app.data.AgentsPasTresNetsSocketImpl
import net.agentspastres.app.domain.model.AccusationFailedEvent
import net.agentspastres.app.domain.model.AccuseEvent
import net.agentspastres.app.domain.model.AskEvent
import net.agentspastres.app.domain.model.Event
import net.agentspastres.app.domain.model.JoinEvent
import net.agentspastres.app.domain.model.ListEvent
import net.agentspastres.app.domain.model.PartyEvent
import net.agentspastres.app.domain.model.StartEvent
import net.agentspastres.app.domain.model.Type
import net.agentspastres.app.domain.model.VictoryEvent
import net.agentspastres.app.domain.model.VoteEvent
import net.agentspastres.app.util.RuntimeTypeAdapterFactory
import net.agentspastres.app.util.ThreeTenTypeAdapters
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level
import org.koin.dsl.module.module
import org.threeten.bp.OffsetDateTime
import org.threeten.bp.format.DateTimeFormatter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


val networkModule = module {

    single { createOkHttpClient() }

    single {
        createWebService<AgentsPasTresNetsService>(
            get(),
            "https://aptn.redissi.xyz/api/",
            get()
        )
    }

    single<AgentsPasTresNetsSocket> { AgentsPasTresNetsSocketImpl() }

    single { createGson() }
}

fun createOkHttpClient(): OkHttpClient {
    val logging = HttpLoggingInterceptor()
    logging.level = Level.BODY

    return OkHttpClient.Builder()
        .connectTimeout(60L, TimeUnit.SECONDS)
        .readTimeout(60L, TimeUnit.SECONDS)
        .addInterceptor(logging)
        .build()
}

inline fun <reified T> createWebService(okHttpClient: OkHttpClient, url: String, gson: Gson): T {

    val retrofit = Retrofit.Builder()
        .baseUrl(url)
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create()).build()
    return retrofit.create(T::class.java)
}

fun createGson(): Gson {
    val offsetDateTimeAdapter = ThreeTenTypeAdapters.create(
        OffsetDateTime::class.java,
        DateTimeFormatter.ISO_OFFSET_DATE_TIME
    )

    val runtimeTypeAdapterFactory = RuntimeTypeAdapterFactory
        .of<Event>(Event::class.java, "type")
        .registerSubtype(PartyEvent::class.java, "party")
        .registerSubtype(JoinEvent::class.java, Type.JOIN.name)
        .registerSubtype(StartEvent::class.java, Type.START.name)
        .registerSubtype(AskEvent::class.java, Type.ASK.name)
        .registerSubtype(AccuseEvent::class.java, Type.ACCUSE.name)
        .registerSubtype(VoteEvent::class.java, Type.VOTE_EVOL.name)
        .registerSubtype(AccusationFailedEvent::class.java, Type.ACCUSATION_FAILED.name)
        .registerSubtype(VictoryEvent::class.java, Type.VICTORY.name)
        .registerSubtype(ListEvent::class.java, Type.CONTENT_IS_LIST.name)

    return GsonBuilder()
        .registerTypeAdapter(OffsetDateTime::class.java, offsetDateTimeAdapter)
        .registerTypeAdapterFactory(runtimeTypeAdapterFactory)
        .create()
}