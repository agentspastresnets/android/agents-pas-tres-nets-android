package net.agentspastres.app.domain.model

data class Place(

    val id: Int = -1,
    val name: String = "",
    val picture: String = ""

)