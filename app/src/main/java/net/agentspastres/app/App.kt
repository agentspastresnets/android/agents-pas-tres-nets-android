package net.agentspastres.app

import android.app.Application
import com.jakewharton.threetenabp.AndroidThreeTen
import net.agentspastres.app.di.createModule
import net.agentspastres.app.di.dataModule
import net.agentspastres.app.di.joinModule
import net.agentspastres.app.di.networkModule
import net.agentspastres.app.di.partyModule
import org.koin.android.ext.android.startKoin
import timber.log.Timber
import timber.log.Timber.DebugTree

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(DebugTree())
        }

        AndroidThreeTen.init(this);

        startKoin(this, listOf(networkModule, dataModule, createModule, joinModule, partyModule))
    }
}