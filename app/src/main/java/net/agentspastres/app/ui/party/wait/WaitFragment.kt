package net.agentspastres.app.ui.party.wait

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.uber.autodispose.AutoDispose
import com.uber.autodispose.android.lifecycle.AndroidLifecycleScopeProvider
import kotlinx.android.synthetic.main.fragment_wait.*
import net.agentspastres.app.R
import net.agentspastres.app.ui.base.BaseFragment
import net.agentspastres.app.ui.party.PartyAction
import org.koin.androidx.viewmodel.ext.android.viewModel

class WaitFragment : BaseFragment<WaitViewState>() {

    private val viewModel: WaitViewModel by viewModel()

    private var gameId: Int = -1
    private var userId: Int = -1


    override fun render(viewState: WaitViewState) {
        btStart.visibility = if (viewState.displayStart) View.VISIBLE else View.GONE
        btStart.isEnabled = viewState.enableStart

        tvPlayers.text = viewState.users.joinToString(separator = "\n") { it.name }

        if (viewState.started) {
            findNavController().navigate(
                R.id.action_waitFragment_to_partyFragment2,
                Bundle().apply {
                    putInt("gameId", gameId)
                    putInt("userId", userId)
                }
            )
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_wait, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.stateStream()
            .`as`(AutoDispose.autoDisposable(AndroidLifecycleScopeProvider.from(this)))
            .subscribe(::render)


        gameId = arguments?.getInt("gameId") ?: -1
        userId = arguments?.getInt("userId") ?: -1

        viewModel.setUserId(userId)

        viewModel.process(PartyAction.Init(gameId))
        viewModel.process(PartyAction.GetGame(gameId))

        btStart.setOnClickListener {
            viewModel.process(PartyAction.Start(gameId))
        }
    }

}