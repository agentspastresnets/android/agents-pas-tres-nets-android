package net.agentspastres.app.di

import net.agentspastres.app.ui.create.CreateController
import net.agentspastres.app.ui.create.CreateViewModel
import net.agentspastres.app.ui.create.PlacesListViewModel
import org.koin.androidx.viewmodel.experimental.builder.viewModel
import org.koin.dsl.module.module

val createModule = module {

    factory { CreateController(get()) }
    viewModel<PlacesListViewModel>()

    viewModel<CreateViewModel>()
}