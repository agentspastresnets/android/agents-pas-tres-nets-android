package net.agentspastres.app.ui.party.accuse

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_user.view.*
import net.agentspastres.app.R
import net.agentspastres.app.domain.model.User
import net.agentspastres.app.ui.party.accuse.AccuseAdapter.AccuseViewHolder

class AccuseAdapter(val selectionListener: (user: User) -> Unit) :
    ListAdapter<User, AccuseViewHolder>(DIFF_CALLBACK) {

    private var currentSelectedUser: Int = -1

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<User>() {
            override fun areContentsTheSame(oldItem: User, newItem: User): Boolean {
                return oldItem == newItem
            }

            override fun areItemsTheSame(oldItem: User, newItem: User): Boolean {
                return oldItem.id == newItem.id
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AccuseViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_user, parent, false)
        return AccuseViewHolder(view)
    }

    override fun onBindViewHolder(holder: AccuseViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    fun setSelectedUser(position: Int) {
        val oldSelectedUser = currentSelectedUser
        currentSelectedUser = position
        notifyItemChanged(oldSelectedUser)
        notifyItemChanged(currentSelectedUser)
    }

    inner class AccuseViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(user: User) {
            itemView.rbUserName.text = user.name
            itemView.rbUserName.isChecked = adapterPosition == currentSelectedUser
            itemView.rbUserName.setOnClickListener {
                selectionListener(user)
            }
        }
    }
}