package net.agentspastres.app.ui.join

import io.reactivex.functions.BiFunction
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import net.agentspastres.app.ui.base.BaseViewModel

class JoinViewModel(controller: JoinController) :
    BaseViewModel<JoinAction, JoinResult, JoinViewState>() {

    override var reducer =
        BiFunction<JoinViewState, JoinResult, JoinViewState> { currentState, result ->
            return@BiFunction when (result) {
                is JoinResult.GameJoined -> currentState.copy(gameJoined = result.game)
                is JoinResult.GamesResult -> currentState.copy(games = result.games)
                is JoinResult.GameSelected -> currentState.copy(gameSelected = result.game)
            }
        }

    init {
        controller.resultStream()
            .subscribeOn(Schedulers.single())
            .scan(JoinViewState(), reducer)
            .subscribe(state)
            .addTo(disposeBag)

        controller.subscribeToActions(actions)
            .addTo(disposeBag)
    }
}