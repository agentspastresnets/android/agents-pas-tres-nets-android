package net.agentspastres.app.ui.party

import net.agentspastres.app.domain.model.User
import net.agentspastres.app.ui.base.Action

sealed class PartyAction : Action {
    class Init(val gameId: Int) : PartyAction()
    class Start(val gameId: Int) : PartyAction()
    class GetGame(val gameId: Int) : PartyAction()
    class GetCard(val placeId: Int, val userId: Int) : PartyAction()
    class GetPlayers(val userIds: List<Int>) : PartyAction()
    class NextTurn(val gameId: Int) : PartyAction()
    class AskQuestion(val gameId: Int, val userSelected: Int) : PartyAction()
    object Pause : PartyAction()
    object Resume : PartyAction()
    class Accuse(val user: User) : PartyAction()
    class GetOtherPlayers(val gameId: Int, val userId: Int) : PartyAction()
}