package net.agentspastres.app.ui.party.accuse

import net.agentspastres.app.domain.model.User
import net.agentspastres.app.ui.base.ViewState

data class AccuseViewState(
    val otherPlayers: List<User> = emptyList(),
    val selectedUser: User? = null
) : ViewState