package net.agentspastres.app.di

import net.agentspastres.app.ui.join.JoinController
import net.agentspastres.app.ui.join.JoinViewModel
import org.koin.androidx.viewmodel.experimental.builder.viewModel
import org.koin.dsl.module.module

val joinModule = module {

    factory { JoinController(get()) }
    viewModel<JoinViewModel>()
}