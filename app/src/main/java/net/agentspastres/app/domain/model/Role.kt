package net.agentspastres.app.domain.model

interface Role {
    val place: Place
}

data class Agent(override val place: Place, val job: String) : Role

class Spy(override val place: Place) : Role