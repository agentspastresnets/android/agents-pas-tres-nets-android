package net.agentspastres.app.domain.model

open class Event(type: String)

data class PartyEvent(val users: List<UserEvent>) : Event("party")

data class UserEvent(val id: String, val name: String)

data class JoinEvent(val content: ContentGame) : Event(Type.JOIN.name)

data class ContentGame(val game: Game)
data class ContentList(val list: List<Int>)

data class StartEvent(val content: ContentGame) : Event(Type.START.name)

data class AskEvent(val content: ContentGame) : Event(Type.ASK.name)
data class AccuseEvent(val content: ContentGame) : Event(Type.ACCUSE.name)
data class VoteEvent(val content: ContentGame) : Event(Type.VOTE_EVOL.name)
data class AccusationFailedEvent(val content: ContentGame) : Event(Type.ACCUSATION_FAILED.name)
data class VictoryEvent(val content: ContentGame) : Event(Type.VICTORY.name)
data class ListEvent(val content: ContentList) : Event(Type.CONTENT_IS_LIST.name)

enum class Type {
    JOIN,
    START,
    CONTENT_IS_LIST,
    ASK,
    ACCUSE,
    VOTE_EVOL,
    ACCUSATION_FAILED,
    VICTORY
}

