package net.agentspastres.app.ui.create

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.uber.autodispose.AutoDispose
import com.uber.autodispose.android.lifecycle.AndroidLifecycleScopeProvider
import kotlinx.android.synthetic.main.fragment_create_party.*
import net.agentspastres.app.R
import net.agentspastres.app.R.layout
import net.agentspastres.app.ui.base.BaseFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class CreatePartyFragment : BaseFragment<CreateViewState>() {

    private val viewModel: CreateViewModel by viewModel()

    private var userId: Int = -1

    override fun render(viewState: CreateViewState) {
        if (viewState.game != null) {
            findNavController().navigate(
                R.id.action_createPartyFragment2_to_nav_party,
                Bundle().apply {
                    putInt("gameId", viewState.game.id)
                    putInt("userId", userId)
                })
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(layout.fragment_create_party, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.stateStream()
            .`as`(AutoDispose.autoDisposable(AndroidLifecycleScopeProvider.from(this)))
            .subscribe(::render)

        userId = arguments?.getInt("userId") ?: -1

        btCreate.setOnClickListener {
            viewModel.process(CreateAction.Create(userId))
        }
    }
}
