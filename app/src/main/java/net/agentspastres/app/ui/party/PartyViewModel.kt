package net.agentspastres.app.ui.party

import io.reactivex.functions.BiFunction
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import net.agentspastres.app.ui.base.BaseViewModel
import timber.log.Timber

class PartyViewModel(controller: PartyController) :
    BaseViewModel<PartyAction, PartyResult, PartyViewState>() {
    override var reducer =
        BiFunction<PartyViewState, PartyResult, PartyViewState> { currentState, result ->
            Timber.d(result.toString())
            return@BiFunction when (result) {
                is PartyResult.Card -> currentState.copy(card = result.card)
                is PartyResult.PlaceResult -> currentState.copy(place = result.place)
                is PartyResult.Pause -> currentState.copy(isPaused = true)
                is PartyResult.Resume -> currentState.copy(isPaused = false)
                is PartyResult.CurrentGame -> {
                    if (currentState.players.isEmpty()) {
                        process(PartyAction.GetPlayers(result.game.playersId))
                    }
                    currentState.copy(game = result.game)
                }
                is PartyResult.PlayersResult -> {
                    currentState.copy(players = result.players)
                }
                is PartyResult.SelectablePlayers -> currentState.copy(selectablePlayers = result.playerIds)
                else -> currentState
            }
        }

    init {
        controller.resultStream()
            .subscribeOn(Schedulers.single())
            .scan(PartyViewState(), reducer)
            .subscribe(state)
            .addTo(disposeBag)

        controller.subscribeToActions(actions)
            .addTo(disposeBag)
    }
}