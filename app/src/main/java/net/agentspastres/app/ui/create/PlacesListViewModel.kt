package net.agentspastres.app.ui.create

import io.reactivex.functions.BiFunction
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import net.agentspastres.app.ui.base.BaseViewModel

class PlacesListViewModel(controller: CreateController) :
    BaseViewModel<CreateAction, CreateResult, PlacesListViewState>() {
    override var reducer =
        BiFunction<PlacesListViewState, CreateResult, PlacesListViewState> { currentState, result ->
            return@BiFunction when (result) {
                is CreateResult.PlacesResult -> currentState.copy(places = result.places)
                else -> currentState
            }
        }

    init {
        controller.resultStream()
            .subscribeOn(Schedulers.single())
            .scan(PlacesListViewState(), reducer)
            .subscribe(state)
            .addTo(disposeBag)

        controller.subscribeToActions(actions)
            .addTo(disposeBag)
    }
}