package net.agentspastres.app.ui.party.wait

import io.reactivex.functions.BiFunction
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import net.agentspastres.app.ui.base.BaseViewModel
import net.agentspastres.app.ui.party.PartyAction
import net.agentspastres.app.ui.party.PartyController
import net.agentspastres.app.ui.party.PartyResult

class WaitViewModel(controller: PartyController) :
    BaseViewModel<PartyAction, PartyResult, WaitViewState>() {

    private var userId: Int = -1

    override var reducer =
        BiFunction<WaitViewState, PartyResult, WaitViewState> { currentState, result ->
            return@BiFunction when (result) {
                is PartyResult.Started -> currentState.copy(started = true)
                is PartyResult.CurrentGame -> currentState.copy(displayStart = userId == result.game.creatorId)
                is PartyResult.Waiting -> currentState.copy(displayStart = userId == result.creatorId)
                is PartyResult.PlayersResult -> currentState.copy(
                    enableStart = result.players.size >= 3,
                    users = result.players
                )
                else -> currentState
            }
        }

    init {
        controller.resultStream()
            .subscribeOn(Schedulers.single())
            .scan(WaitViewState(), reducer)
            .subscribe(state)
            .addTo(disposeBag)

        controller.subscribeToActions(actions)
            .addTo(disposeBag)
    }

    fun setUserId(userId: Int) {
        this.userId = userId
    }

}